FROM openjdk:11-slim
ARG JAR_FILE=target/product-app-latest.jar
COPY ${JAR_FILE} product-app-0.0.1-SNAPSHOT.jar
ENTRYPOINT exec java -jar product-app-0.0.1-SNAPSHOT.jar