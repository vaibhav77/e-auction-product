package com.eauction.seller.productapp.service;

import com.eauction.seller.productapp.dto.Bid;
import com.eauction.seller.productapp.dto.ProductBid;
import com.eauction.seller.productapp.dto.ProductDTO;
import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.entity.BuyerBidDetail;
import com.eauction.seller.productapp.entity.ProductDetail;
import com.eauction.seller.productapp.mapper.ProductMapper;
import com.eauction.seller.productapp.repository.BidDetailsRepository;
import com.eauction.seller.productapp.repository.ProductDetailsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Log4j2
public class ProductService {

    private final ProductDetailsRepository productDetailsRepository;
    private final BidDetailsRepository bidDetailsRepository;
    private final ProductMapper productMapper;

    public ProductDetail addNewProduct(ProductDetailsDTO productDetailsDTO) {
        log.debug("Received input: {}", productDetailsDTO);
        if(Objects.nonNull(productDetailsDTO)) {
            ProductDetail productDetail = productMapper.fromProductDetailsDTO(productDetailsDTO);
            productDetailsRepository.createProduct(productDetail);
            return productDetail;
        }
        return null;
    }

    public List<ProductDTO> fetchProducts() {
        List<ProductDetail> productDetails = productDetailsRepository.fetchAllProducts();
        return productMapper.fromProductDetails(productDetails);
    }

    public ProductBid fetchProductBids(String productId) {
        ProductDetail productDetail = productDetailsRepository.fetchProductById(productId);
        List<BuyerBidDetail> bidDetails = bidDetailsRepository.fetchBids(productId);
        return populateProductBidDetails(productDetail,bidDetails);
    }

    private ProductBid populateProductBidDetails(ProductDetail productDetail,List<BuyerBidDetail> bidDetails) {
        ProductBid productBid  = productMapper.fromProductDetails(productDetail);
        productBid.setBids(productMapper.fromBidDetails(bidDetails));
        Comparator<Bid> comparator = Comparator.comparingLong(bid-> Long.valueOf(bid.getBidAmount()));
        productBid.getBids().sort(comparator.reversed());
        return productBid;
    }
}