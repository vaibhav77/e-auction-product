package com.eauction.seller.productapp.controller;

import com.eauction.seller.productapp.dto.ProductBid;
import com.eauction.seller.productapp.dto.ProductDTO;
import com.eauction.seller.productapp.dto.ProductDetailsDTO;
import com.eauction.seller.productapp.entity.ProductDetail;
import com.eauction.seller.productapp.exception.InvalidProductInputsException;
import com.eauction.seller.productapp.service.ProductService;
import com.eauction.seller.productapp.validation.ValidationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = {"http://localhost:3000","http://localhost:8082"})
@RestController
@RequiredArgsConstructor
@RequestMapping("/seller")
public class ProductController {

    private final ProductService productService;
    private final ValidationService validationService;

    @PostMapping ("/add-product")
    public ResponseEntity<ProductDetail> addProduct(@Valid @RequestBody ProductDetailsDTO productDetailsDTO) {
        validationService.validateProductInputs(productDetailsDTO);
        return Optional.ofNullable(productService.addNewProduct(productDetailsDTO))
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping ("/products")
    public ResponseEntity<List<ProductDTO>> fetchProducts() {
        return Optional.ofNullable(productService.fetchProducts())
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/show-bids/{productId}")
    public ResponseEntity<ProductBid> fetchProductBids(@PathVariable("productId") String productId) {
        return Optional.ofNullable(productService.fetchProductBids(productId))
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @ExceptionHandler(InvalidProductInputsException.class)
    public ResponseEntity<String> handleProductException(InvalidProductInputsException businessRuleServiceException){
        return new ResponseEntity<>(businessRuleServiceException.getMessage(),HttpStatus.BAD_REQUEST);
    }

}